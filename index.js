(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define('AjaxSource', ['qw', 'Ajax', 'DataView'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        module.exports = factory(require('qw'), require('Ajax'), require('DataView'));
    } else {
        root.AjaxSource = factory(root.qw, root.Ajax, root.DataView);
    }
})(this, function(qw, Ajax, DataView) {

    var validMethods = ['GET', 'POST'];

    function AjaxSource()
    {
        this._dataview = null;
        this._request_instance = null;
    }

    AjaxSource.prototype.setDataView = function(dataview) {
        this._dataview = dataview;
        return this;
    };

    AjaxSource.prototype.destroy = function() {
        if (this._request_instance) {
            this._request_instance.abort();
        }
        delete this._dataview;
        delete this._request_instance;
    };

    AjaxSource.prototype.request = function(request_data) {
        if (!qw.isPlainObject(request_data)) {
            request_data = {};
        }

        var that = this, dataview = this._dataview,
            url = dataview.getOption('url', ''),
            method = dataview.getOption('requestMethod'),
            extraData = dataview.getOption('extraData');

        if (!method || !qw.isString(method) || qw.indexOf(validMethods, method.toUpperCase()) === -1) {
            method = 'POST';
        } else {
            method = method.toUpperCase();
        }
        if (qw.isPlainObject(extraData)) {
            qw.extend(request_data, extraData);
        }

        dataview.disable();
        this._request_instance = Ajax({
            url: url,
            method: method,
            data: request_data,
            dataType: 'json'
        }).done(function(responseJson, request, response) {
            // veri dizi şeklinde değilse paskırt.
            if (!responseJson || !qw.hasOwn(responseJson, 'data') || !qw.isArray(responseJson.data)) {
                dataview.trigger('response_error', 'Received data is not exist or is not in valid format.', responseJson, response.status());
                return;
            }
            if (!responseJson || !qw.hasOwn(responseJson, 'options') || !qw.isObject(responseJson.options)) {
                dataview.trigger('response_error', 'Received options data is not exist or is not in valid format.', responseJson, response.status());
                return;
            }

            var responseData = responseJson.data;
            var optionsData = responseJson.options;

            // response_success_before eventinin data'yı ve options'u değiştirme hakkı vardır.
            dataview.trigger('response_success_before', responseData, optionsData, response.status());
            dataview.prepareForResponse(optionsData);
            // responseData içindeki tüm veriler tek tek ekleniyor.
            qw.each(responseData, function(rowData) {
                dataview.addRow(rowData);
            });
            // success_after tetikleniyor.
            dataview.trigger('response_success_after', responseData, optionsData, response.status());
        }).error(function(responseData, request, response) {
            dataview.trigger('response_error', 'An error occured.', responseData, response.status());
        }).always(function(responseData, request, response) {
            dataview.enable();
            dataview._request_count++;
            // evet, response_finish olayı içinden ilk istek mi diye sorgulamak isteyen
            // kullanıcı dataview.getRequestCount() === 1 kontrolü yapacak.
            // ancak response_success_(before|after), response_error içinde ilk istek olup olmadığını
            // anlamak için dataview.getRequestCount() === 0 kontrolü yapılacak.
            dataview.trigger('response_finish', responseData, response.status());
            that._request_instance = null;
        });
    };
    return AjaxSource;
});
